@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card ">

                    <div class="card-header">
                        <h2>
                            {{ $article->title }}
                        </h2>
                        <p>
                            {{ __('articles.by') }} <strong><i class="fas fa-user-alt"></i> {{ $article->writer->name }}
                            </strong>, <i
                                    class="fas fa-clock"></i> {{ $article->created_at->diffForHumans() }}
                        </p>
                    </div>
                    <div class="card-body">
                        {{ $article->body }}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <h1>{{ __('articles.comments') }}</h1>
                        </div>
                        <div class="comments-list">
                            @foreach($replies as $reply)
                                <div class="card">
                                    <div class="card-body">
                                        {{ $reply->body }}
                                    </div>
                                    <p class="commentauthor text-muted">
                                        @if($reply->owner)
                                            <i class="fas fa-user-alt"></i> {{ $reply->owner->name }},
                                        @else()
                                            {{ __('articles.byGuest') }},
                                        @endif
                                        <i class="fas fa-clock"> </i>
                                        {{ $reply->created_at->diffForHumans() }}
                                    </p>
                                </div>
                            @endforeach

                        </div>
                        <br>
                        {{ $replies->links() }}
                        <br>
                        <form action="{{ $article->path() . '/replies' }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                            <textarea name="body" id="body" cols="30" rows="5" class="form-control"
                                      placeholder="Have something to say?"></textarea>
                            </div>
                            <button type="submit" class="btn btn-default">Post</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
