@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('articles.title')}}</div>

                    <div class="card-body">
                        @if(!count($articles))
                            <article>
                                <div class="body">
                                    {{ __('articles.messages.empty') }}
                                </div>
                            </article>
                        @endif
                        @foreach($articles as $article)
                            <article>
                                <div class="level">
                                    <h4 class="flex">
                                        <a href="{{ $article->path() }}">
                                            {{ $article->title }}
                                        </a>
                                    </h4>
                                </div>
                                <div class="body">
                                    {{ $article->body }}
                                </div>
                            </article>
                            <hr>
                        @endforeach
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
