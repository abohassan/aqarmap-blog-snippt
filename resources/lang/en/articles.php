<?php

return [
    'title' => 'Blog Articles',
    'messages' => [
        'empty' => 'There are no records right now, please check later.'
    ],
    'fields' => [
        'title' => 'Title',
        'body' => 'Body',
        'category' => 'Category',
    ],
    'createTitle' => 'Create new article',
    'categories' => 'Categories',
    'byGuest' => 'By a guest',
    'comments' => 'Comments',
    'by' => 'By'
];
