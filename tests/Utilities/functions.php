<?php
/**
 * Created by PhpStorm.
 * User: Mohamed Abohassan
 * Date: 10/12/2018
 * Time: 5:01 PM
 */

/**
 * Make object of a class.
 * @param $class
 * @param array $attributes
 * @return mixed
 */
function make($class, $attributes = [])
{
    return factory($class)->make($attributes);
}

/**
 * Create object of a class.
 * @param $class
 * @param array $attributes
 * @return mixed
 */
function create($class, $attributes = [])
{
    return factory($class)->create($attributes);
}
