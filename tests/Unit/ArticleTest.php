<?php

namespace Tests\Unit;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    protected $article;

    protected function setUp()
    {
        parent::setUp();
        $this->article = create(Article::class);
    }

    /** @test */
    public function article_should_have_category()
    {
        $this->assertInstanceOf(Category::class, $this->article->category);
    }

    /** @test */
    public function article_should_have_writer()
    {
        $this->assertInstanceOf(User::class, $this->article->writer);
    }

    /** @test */
    public function article_should_have_replies()
    {
        $this->assertInstanceOf(Collection::class, $this->article->replies);
    }
}
