<?php

namespace Tests\Unit;

use App\Replay;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplayTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function replay_may_have_owner()
    {
        $replay = create(Replay::class, [
            'user_id' => create(User::class)->id
        ]);
        $this->assertInstanceOf(User::class, $replay->owner);
    }
}
