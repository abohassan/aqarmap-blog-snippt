<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticlesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_can_list_articles()
    {
        $article = create(Article::class);

        $response = $this->get('/');

        $response->assertStatus(200)
            ->assertSee($article->title);
    }

    /** @test */
    public function a_user_can_view_article_by_id()
    {
        $article = create(Article::class);
        $this->get($article->path())
            ->assertSee($article->title);
    }

    /** @test */
    public function guests_cannot_create_articles()
    {
        // cannot visit create forum page.
        $this->get('/articles/create')
            ->assertStatus(403);

        // cannot send create forum request.
        $this->post('/articles/create', [])
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_add_articles()
    {
        $user = create(User::class, ['type' => 'administrator']);
        $this->signIn($user);

        $article = make(Article::class);
        $respond = $this->post('/articles/create', $article->toArray())
            ->assertSessionHasNoErrors();

        $this->get($respond->headers->get('Location'))
            ->assertSee($article->title)
            ->assertSee($article->body);
    }

    /** @test */
    public function it_required_a_title()
    {
        $this->publishArticle(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function it_required_a_body()
    {
        $this->publishArticle(['body' => null])
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function it_required_a_valid_chanel_id()
    {
        $this->publishArticle(['category_id' => null])
            ->assertSessionHasErrors('category_id');

        $this->publishArticle(['category_id' => 99999])
            ->assertSessionHasErrors('category_id');

    }

    public function publishArticle($overrides = [])
    {
        $user = create(User::class, ['type' => 'administrator']);
        $this->signIn($user);
        $thread = make(Article::class, $overrides);
        return $this->post('/articles/create', $thread->toArray());
    }


    /** @test */
    public function visitor_can_filter_articles_by_category()
    {
        $category = create(Category::class);

        $articleInCategory = create(Article::class, ['category_id' => $category->id]);
        $articleNotInCategory = create(Article::class);

        $this->get("/?category={$category->id}")
            ->assertSee($articleInCategory->title)
            ->assertDontSee($articleNotInCategory->title);

    }
}
