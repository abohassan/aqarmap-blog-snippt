<?php
/**
 * Created by PhpStorm.
 * User: Mohamed Abohassan
 * Date: 10/12/2018
 * Time: 7:35 PM
 */

namespace Tests\Feature;


use App\Article;
use App\Replay;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RepliesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_read_replies_associated_with_an_article()
    {
        $article = create(Article::class);

        $replay = create(Replay::class, ['article_id' => $article->id]);
        $this->get($article->path())->assertSee($replay->body);
    }

    /** @test */
    public function a_visitor_may_participate_in_an_article()
    {
        $article = create(Article::class);
        $replay = make('App\Replay');

        $this->post($article->path() . '/replies', $replay->toArray());

        $this->get($article->path())->assertSee($replay->body);
    }

    /** @test */
    public function replay_required_a_body()
    {
        $article = create(Article::class);

        $replay = make('App\Replay', ['body' => null]);
        $this->post($article->path() . '/replies', $replay->toArray())
            ->assertSessionHasErrors('body');
    }
}