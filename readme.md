# Aqarmap-blog-snippt

A simple blogging system.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Follow the instruction of how you create a [laravel app](https://laravel.com/docs/5.7).

### Installing

You need to
1. rename ".env.example" to ".env" in the root directory.
2. generate you app key with
```
php artisan key:generate
```
3. Run composer to install project dependencies.
```
composer install
```
4. create your database
5. change variables in "{{}}" with your database details.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={{DATABASE_NAME}}
DB_USERNAME={{DATABASE_USER_NAME}}
DB_PASSWORD={{DATABASE_USER_PASSWORD}}
```

After that you will need to run migrations

```
php artisan migrate
```

Then run the seeder to make an admin account for you

```
php artisan db:seed
```

To run the server and start blogging run :
```
php artisan serve
```

Here you go, you now can have full access to the system With these details
```
email: admin@adminpanel.com
password: secret
```
## Running the tests

To make sure that all functionality of the system run as expected please run test cases

```
./vendor/bin/phpunit 
```
