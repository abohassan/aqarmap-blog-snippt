<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\Articles\CreateReplay;
use App\Replay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepliesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Article $article
     * @param  CreateReplay $request
     * @return \Illuminate\Http\Response
     */
    public function store(Article $article, CreateReplay $request)
    {

        Replay::create([
            'body' => \request('body'),
            'article_id' => $article->id,
            'user_id' => (Auth::check()) ? Auth::user()->id : null
        ]);
        return redirect($article->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
