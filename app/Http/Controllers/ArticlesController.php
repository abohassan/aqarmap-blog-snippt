<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Http\Requests\Articles\BrowseCreateArticle;
use App\Http\Requests\Articles\CreateArticle;
use Illuminate\Http\Request;

class ArticlesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::filter()->paginate();

        return view('articles/index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param BrowseCreateArticle $request
     * @return \Illuminate\Http\Response
     */
    public function create(BrowseCreateArticle $request)
    {
        $categories = Category::all();
        return view('articles/create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateArticle $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticle $request)
    {
        $articleDetails = \request()->only(['title', 'body', 'category_id']);

        $articleDetails['created_by'] = auth()->id();


        $article = Article::create($articleDetails);

        return redirect($article->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {

        return view('articles/view', [
            'article' => $article,
            'replies' => $article->replies()->paginate()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
