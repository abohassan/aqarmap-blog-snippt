<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
        View::share('categories', Category::all());
    }
}
