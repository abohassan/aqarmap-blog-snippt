<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'category_id', 'created_by'
    ];

    public function scopeCategory(Builder $builder, $categoryId)
    {
        $builder->where('category_id', $categoryId);
        return $this;
    }


    public function path()
    {
        return "/articles/{$this->id}";
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function writer()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function replies()
    {
        return $this->hasMany(Replay::class);
    }

    public function scopeFilter($query)
    {
        if (request('category')) {
            $category = Category::find(request('category'));
            if ($category->exists) $query->where('category_id', $category->id);
        }
    }
}
